const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

function makeList(arr, parent) {
    const ul = document.createElement('ul')
    document.querySelector(parent).append(ul)

    arr.forEach((item) => {
        try {

            if (!item.hasOwnProperty("name")) {
                throw new Error(" свойства name нет ")
            } else if (!item.hasOwnProperty("author")) {
                throw new Error(" свойства author нет ")
            } else if (!item.hasOwnProperty("price")) {
                throw new Error(" свойства price нет ")
            } else {
                let apItem = document.createElement("li");
                apItem.innerText = `Autor: ${item.author} Name: ${item.name} Price ${item.price}`
                apItem.style.cssText = 'list-style-type: none;' +
                    'font-size: 18px;' +
                    'font-weight: 700;'
                console.log(item.author)
                console.log(item.name)
                console.log(item.price)
                ul.append(apItem)
            }

        } catch (error) {
            console.error(error)
        }
    })
}
makeList(books, "#root");


//Теоретический вопрос
// Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch.
//Ответ:
// Когда нужно сгенерировать собственную ошибку,когда нужно проверить данные на корректность