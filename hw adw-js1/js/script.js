"use strict"

class Employee {
    constructor(name, age, salary) {
        this._name = name
        this._age = age
        this._salary = salary
    }

    get getName(){
        return this._name
    }
    get getAge(){
        return this._age
    }

    get getSalary(){
        return this._salary
    }

    set setName(anotherName){

        return this._name=anotherName
    }
    set setAge(anotherAge){
        return this._age=anotherAge
    }
    set setSalary(anotherSalary){
        return this._salary=anotherSalary
    }
}


class Programer extends Employee{
    constructor(name, age, salary, langs) {
        super(name, age, salary);
        this._langs=langs
    }

    get getAnotherSalary() {
        return this.getSalary * 3
    }
}


const junior =new Programer("ivan",33,333,['JS', "Python", "Java"])
const midel=new Programer("vasya23",23,555,["English","Russian","Ukraine"])
console.log(junior)
console.log(midel)


// Теоретический вопрос
// Обьясните своими словами, как вы понимаете, как работает прототипное наследование в Javascript
// Каждый объект имеет свой прототип  и может унаследовать свойства родитель и тд в полоть до глобального родителя
