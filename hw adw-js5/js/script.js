const btn = document.querySelector(".btn-ip");
const url = 'https://api.ipify.org/?format=json';
const userInfo = document.querySelector(".userInfo");

userInfo.style="padding-inline-start:0px";
btn.style ="cursor:pointer";


async function getIpAdress() {
    const response = await fetch(url);
    const data = await response.json();
    return data.ip
}

async function getFullAdress(IP) {
    const response = await fetch(`http://ip-api.com/json/${IP}?fields=61438`);
    const data = await response.json();
    // return data
    console.log(data);

    const responseData = await data;

    const {timezone, countryCode, region, regionName, city} = responseData;

    for (let property in responseData) {
        userInfo.innerHTML = `
                <li>Континент: ${timezone}</li>
                <li>Страна: ${countryCode}</li>
                <li>Регион: ${region}</li>
               <li>Район: ${regionName}</li>
               <li>Регион города: ${city}</li>
        `
    }
}

    btn.addEventListener("click", async function () {
        await getFullAdress(await getIpAdress());
    });


// Обьясните своими словами, как вы понимаете асинхронность в Javascript
// Это возможность путем определенных команд добиться выпонения кода в том месте где это необходимо так как код JS читаеться синхронно







